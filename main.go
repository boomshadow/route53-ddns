package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net"
	"net/http"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/route53"
	"github.com/caarlos0/env"
)

type config struct {
	Domain            string `env:"DDNS_DOMAIN,required"`
	Zone_ID           string `env:"ROUTE53_ZONE_ID,required"`
	Access_Key_ID     string `env:"AWS_ACCESS_KEY_ID"`     // Optional, if not specified, AWS SDK will look for creds in ~/.aws/credentials
	Secret_Access_Key string `env:"AWS_SECRET_ACCESS_KEY"` // Optional, if not specified, AWS SDK will look for creds in ~/.aws/credentials
}

func main() {
	// Gather ENV vars and make sure we have everything we need
	cfg := config{}
	err := env.Parse(&cfg)
	if err != nil {
		fmt.Printf("%+v\n", err)
	}
	// Print all args (debugging)
	// fmt.Printf("%+v\n", cfg)

	// Initialize AWS session
	sess, err := session.NewSession()
	if err != nil {
		log.Fatal("failed to create session,", err)
		return
	}
	svc := route53.New(sess)
	current_ip := get_current_ip()
	route53_ip := get_route53_dns(svc, cfg)

	if current_ip == route53_ip {
		log.Print("No IP change needed at this time.")
	} else {
		log.Print("Updating IP from ", route53_ip, ", to: ", current_ip)
		update_route53_dns(svc, cfg, current_ip)
	}
}

type Current_ip_json struct {
	IP string `json:"ip"`
}

var current_ip_json Current_ip_json

func get_current_ip() string {
	response, err := http.Get("https://boomshadow.net/ip")
	if err != nil {
		log.Fatal(err)
	}
	ip_Json, err := ioutil.ReadAll(response.Body)
	response.Body.Close()
	if err != nil {
		log.Fatal(err)
	}

	json.Unmarshal(ip_Json, &current_ip_json)
	// Make sure we have a valid IP address
	if net.ParseIP(current_ip_json.IP) == nil {
		log.Fatal("The current IP is not a valid IP address: ", current_ip_json.IP)
	}
	return current_ip_json.IP
}

func get_route53_dns(svc *route53.Route53, cfg config) string {
	listParams := &route53.ListResourceRecordSetsInput{
		HostedZoneId:    aws.String(cfg.Zone_ID),
		MaxItems:        aws.String("1"),
		StartRecordName: aws.String(cfg.Domain),
		StartRecordType: aws.String("A"),
	}
	response, err := svc.ListResourceRecordSets(listParams)

	if err != nil {
		log.Fatal(err.Error())
	}
	return *response.ResourceRecordSets[0].ResourceRecords[0].Value
}

func update_route53_dns(svc *route53.Route53, cfg config, current_ip string) {
	params := &route53.ChangeResourceRecordSetsInput{
		ChangeBatch: &route53.ChangeBatch{
			Changes: []*route53.Change{
				{
					Action: aws.String("UPSERT"),
					ResourceRecordSet: &route53.ResourceRecordSet{
						Name: aws.String(cfg.Domain),
						Type: aws.String("A"),
						ResourceRecords: []*route53.ResourceRecord{
							{
								Value: aws.String(current_ip),
							},
						},
						TTL: aws.Int64(60),
					},
				},
			},
		},
		HostedZoneId: aws.String(cfg.Zone_ID),
	}
	response, err := svc.ChangeResourceRecordSets(params)

	if err != nil {
		log.Fatal(err.Error())
	}

	log.Print("Successfully submitted change request! Status: ", *response.ChangeInfo.Status)
}
