[![forthebadge](https://forthebadge.com/images/badges/uses-badges.svg)](http://forthebadge.com)
[![forthebadge](https://forthebadge.com/images/badges/powered-by-electricity.svg)](https://forthebadge.com)
[![forthebadge](https://forthebadge.com/images/badges/compatibility-betamax.svg)](https://forthebadge.com)

# About
<https://gitlab.com/boomshadow/route53-ddns>  
Route53 Dynamic DNS client, written in Golang

Leverage your own AWS Route53 Zone for Dynamic DNS purposes. Run this client on your home servers, laptops, etc... to have a particular Route53 record always updated with your current public-facing IP address.

# Downloads
Pre-made binaries are created with each release. You can find the files as part of the Git tags here:

<https://gitlab.com/boomshadow/route53-ddns/tags>

# Running the binary
Once you have the `route53-ddns` binary on your system, simply run it with the required environment variables:

    DDNS_DOMAIN="example-ddns.domain.com" ROUTE53_ZONE_ID="X1XXX1XXX11X1X" /path/to/route53-ddns

Be sure to put that on a cronjob (every 10 minutes should be frequent enough).

## AWS Credentials
By default, the AWS SDK looks for credentials in `~/.aws/credentials` (Linux/Mac). If you want to override that, you can set the credentials with env variables when you run the binary alongside the other env vars:

    AWS_ACCESS_KEY_ID="" AWS_SECRET_ACCESS_KEY="" DDNS_DOMAIN="" ROUTE53_ZONE_ID="" /path/to/route53-ddns

Details on Amazon secret handling for the SDK can be found here: 
- <https://aws.amazon.com/blogs/security/a-new-and-standardized-way-to-manage-credentials-in-the-aws-sdks/>
- <https://docs.aws.amazon.com/sdk-for-ruby/v3/developer-guide/setup-config.html#aws-ruby-sdk-setting-region>

## IAM Policy
This is an example IAM policy that you can apply to an user or role:

```json
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "route53:ChangeResourceRecordSets",
        "route53:ListResourceRecordSets"
      ],
      "Effect": "Allow",
      "Resource": "arn:aws:route53:::hostedzone/ROUTE53_ZONE_ID_GOES_HERE"
    }
  ]
}
```

Unfortunately, it's not possible to restrict access to specific resource record, but as a workaround, you could create a new 'hosted zone' for a `ddns` sub-domain. (Ex: device.ddns.domain.com)

# Building

## Dependencies
- golang (duh)
- dep (https://golang.github.io/dep/docs/installation.html)
- goreleaser (only needed for building _all_ the binaries -- https://goreleaser.com/install/)

Use `dep` to install the dependencies like so:

    dep ensure

## Compiling

    go build
    # For building all the binaries:
    goreleaser release --skip-publish

# Copyright
This is free and unencumbered software released into the public domain.  
See LICENSE.md or [unlicense.org](http://unlicense.org) for more information.
